import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(5);
        Rectangle rectangle = new Rectangle(10, 15);

        System.out.println("Circle: ");
        System.out.println(circle.toString());
        System.out.println("s = " + circle.getArea());
        System.out.println("p = " + circle.getPerimeter());


        System.out.println("Rectangle: ");
        System.out.println(rectangle.toString());
        System.out.println("s = " + rectangle.getArea());
        System.out.println("p = " + rectangle.getPerimeter());
    }
}
